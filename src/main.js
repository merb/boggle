import { createApp } from 'vue'
import App from './Root.vue'

createApp(App).mount('#app')
