module.exports = cfg => {
  const dev = cfg.env === 'development'

  return {
    map: dev ? { inline: false } : false,
    parser: false,
    plugins: [
      require('postcss-nested')(),
      // require('postcss-assets')({
      //   loadPaths: ['src/assets/']
      // }),
      require('autoprefixer')(),
      dev ? null : require('cssnano')()
    ]

  }
}
