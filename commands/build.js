'use strict'
// -------------------
// Generate server ready files
// ready to be shipped
// -------------------

// Get env variables
require('dotenv').config({
  path: './.env'
})

const chalk = require('chalk')
const log = console.log
const buildServer = require('./functions/prepare-shipment')

buildServer()
  .then(res => {
    log(chalk.bgGreen(res))
  })
  .catch(err => {
    console.error(chalk.bgRed(err))
  })
